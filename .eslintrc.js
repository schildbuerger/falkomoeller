module.exports = {
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    browser: true,
    node: true,
    jest: true
  },
  globals: {
    expect: true
  },
  extends: [
    'prettier',
    'plugin:vue/recommended',        // /base, /essential, /strongly-recommended, /recommended
    'plugin:prettier/recommended',   // turns off all ESLINT rules that are unnecessary due to Prettier or might conflict with Prettier. 
    'eslint:recommended'
  ],
  plugins: ['vue', 'prettier'],
  rules: {
    'vue/max-attributes-per-line': 'off',
    'prettier/prettier': [            // customizing prettier rules (not many of them are customizable)
      'error',
      {
        singleQuote: true,
        semi: false,
        tabWidth: 2
      },
    ],
    'no-console': 'off'
  }
}

/* {
  // VSCode User Settings for ESLint!!!
  // The number of spaces a tab is equal to. This setting is overridden based on the file contents when `editor.detectIndentation` is on.
  "editor.tabSize": 2,
  // Turns auto fix on save on or off.
  "eslint.autoFixOnSave": true,
  // An array of language ids which should be validated by ESLint
  "eslint.validate": [
    "javascript",
    {
      "language": "vue",
      "autoFix": true
    },
    "javascriptreact",
    "vue"
  ],
  "editor.formatOnSave": true,
  "[javascript]": {
    "editor.formatOnSave": false
  },
  "eslint.alwaysShowStatus": true
} */